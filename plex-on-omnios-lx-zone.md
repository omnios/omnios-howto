Credit: [Mike Zeller's Plex on SmartOS](http://lightsandshapes.com/plex-on-smartos/)

Thanks also to [gea's napp-it Zones - Setup on OmniOS](http://www.napp-it.org/doc/downloads/zones.pdf)


# Create LX zone

Ready Images by Joyent: https://docs.joyent.com/public-cloud/instances/infrastructure/images

Click on a link to check uuid and proper kernel setting (in the json output)

For example, in case of latest Ubuntu 16.04 image available:
https://images.joyent.com/images/7b5981c4-1889-11e7-b4c5-3f3bdfc9b88b

One can see the kernel value is `4.3.0`.

Append "file" to the link to download image:
https://images.joyent.com/images/7b5981c4-1889-11e7-b4c5-3f3bdfc9b88b/file

## Create virtual NIC

```
$ uname -a
SunOS lisp 5.11 omnios-r151028-d3d0427bff i86pc i386 i86pc Solaris

# If not already there, create dataset just for zones
$ zfs create -o mountpoint=/zone rpool/zone

# Create a virtual NIC (VNIC) for the zone

$ dladm show-link
LINK        CLASS     MTU    STATE    BRIDGE     OVER
igb0        phys      1500   up       --         --

$ dladm create-vnic lx0 -l igb0

$ dladm show-link
LINK        CLASS     MTU    STATE    BRIDGE     OVER
igb0        phys      1500   up       --         --
lx0         vnic      1500   up       --         igb0

```



## List current zones
```
$ zoneadm list -vi
    ID NAME             STATUS     PATH                           BRAND    IP
     0 global           running    /                              ipkg     shared
```

~~## Prepare dataset for new zone~~

    ```
    $ zfs create rpool/zone/ubuntu-16.04
    $ chmod -R 700 /zone/ubuntu-16.04
    ```

Note: There is no need to prepare a dataset. `zoneadm install` will automatically create one with proper permissions.


## Add extra dataset to hold media files

This dataset will be mounted via `lofs` by the zone. 

```
zfs create -o mountpoint=/store rpool/store
```

## Configure zone by importing settings

```
$ zonecfg -z ubuntu-16.04 -f ubuntu-16.04.cfg
```

Where `ubuntu-16.04.cfg` is:

```
$ cat ubuntu-16.04.cfg

create -b
set zonepath=/zone/ubuntu-16.04
set brand=lx
set autoboot=false
set ip-type=exclusive
add fs
set dir=/store
set special=/store
set type=lofs
end
add net
set physical=lx0
add property (name=gateway,value="192.168.1.1")
add property (name=ips,value="192.168.1.70/24")
add property (name=primary,value="true")
end
add attr
set name=dns-domain
set type=string
set value=ubuntu
end
add attr
set name=resolvers
set type=string
set value=192.168.1.1
end
add attr
set name=kernel-version
set type=string
set value=4.3.0
end
```

### Or else, create manually using same values as above,


```
$ zonecfg -z ubuntu-16.04

create -b

[Type values above, one by one]
```


## Download and install Ubuntu 16.04 zfs dataset

```
$ curl -o ubuntu-16.04.zss.gz https://images.joyent.com/images/7b5981c4-1889-11e7-b4c5-3f3bdfc9b88b/file

# Install

$ zoneadm -z ubuntu-16.04 install -s /root/ubuntu-16.04.zss.gz
    A ZFS file system has been created for this zone.
```

## Boot zone

```
$ zoneadm -z ubuntu-16.04 boot

```

## Login into zone

To to give password to a user (i.e. root), properly edit sshd_config to ssh from other workstation.

```
$ zlogin ubuntu-16.04

[Connected to zone 'ubuntu-16.04' pts/5]
Welcome to Ubuntu 16.04.2 LTS (GNU/Linux 4.3.0 x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

The programs included with the Ubuntu system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Ubuntu comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
applicable law.

   __        .                   .
 _|  |_      | .-. .  . .-. :--. |-
|_    _|     ;|   ||  |(.-' |  | |
  |__|   `--'  `-' `;-| `-' '  ' `-'
                   /  ;  Instance (Ubuntu 16.04 20170403)
                   `-'   https://docs.joyent.com/images/container-native-linux

```


## Install Plex

```
$ wget wget https://downloads.plex.tv/plex-media-server/1.7.5.4035-313f93718/plexmediaserver_1.7.5.4035-313f93718_amd64.deb

$ dpkg -i plexmediaserver_1.7.5.4035-313f93718_amd64.deb

$ systemctl enable plexmediaserver
$ systemctl start plexmediaserver

```

Then, connect to the web server by tunnel:

```
ssh -L32400:127.0.0.1:32400 root@192.168.1.70
```

## Result

Screenshot from Firefox, address http://127.0.0.1:32400/web/

![screenshot 2018-12-15 at 17 07 31](https://user-images.githubusercontent.com/1652143/50044976-fdcc7580-008b-11e9-95b8-74913b4450bd.png)
